﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using System.Collections;

namespace JSON_PARSER
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
      oPENToolStripMenuItem.Enabled = true;
      btnOpenTxt.Enabled = false;
      btnCompareItem.Enabled = false;
      SaveJson.Enabled = false;
    }


    public static string DB_PATH = @"";
    public static string TXT_PATH = @"";

    string JsonFileName;
    string TxtFileName;

    List<string> JsonCollection = new List<string>();
    List<string> TxtCollection = new List<string>();
    JArray a;
    




    #region Methods


    //Сохранение
    public void Save()
    {
      try
      {
        //Сохраняем первую строку с названиями колонок
        for (int k = 0; k < dataGridView1.ColumnCount; k++)
          myExcel.App.Cells[1, k + 1] = dataGridView1.Columns[k].HeaderText;

        for (int i = 0; i < dataGridView1.Rows.Count; i++)//Проходимся по строкам
        {
          for (int j = 0; j < dataGridView1.ColumnCount; j++)//Проходимся по колонкам
          {
            myExcel.App.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value;
          }
        }
        myExcel.App.Visible = false;

        //myExcel.Book.Save();


      }
      catch (Exception ex)
      {

        MessageBox.Show(ex.Message);
      }

    }


    //Фильтр
    private void Filter()
    {
      try
      {
        this.dataGridView1.ClearSelection();
        foreach (DataGridViewRow r in this.dataGridView1.Rows)
        {
          for (int i = 0; i < dataGridView1.ColumnCount; i++)
          {
            if (r.Cells[i].Value != null)
            {
              if ((r.Cells[i].Value).ToString().StartsWith(this.txtbxSearch.Text.Trim()))
              {
                this.dataGridView1.Rows[r.Index].Selected = true;
              }
            }
          }

        }

      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace);
      }

    }



    void Json_worker()
    {
      var json = System.IO.File.ReadAllText(DB_PATH);

      dynamic Data = JToken.Parse(json);


      dynamic devices = Data[0];


      a = (JArray)devices["devices"];
      List<string> arr = new List<string>();
      foreach (var item in a)
      {
        string Id = null;
        for (int j = 0; j < dataGridView1.Rows.Count; j++)
          Id = Convert.ToString(dataGridView1.Rows[j].Cells[0].Value = j + 1);
        arr.Add(Convert.ToString(item));

        var value = item;
        var arr1 = value[0];
        var arr2 = value[1];
        var arr3 = value[2];

        JsonCollection.Add(arr2.ToString());

        dataGridView1.Rows.Add(Id, value, arr1, arr2, arr3);
      }

    }
    private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
    {
      e.Row.HeaderCell.Value = (e.Row.Index + 1).ToString();
    }

    #endregion


    //Открытие файла JSON
    private void oPENToolStripMenuItem_Click(object sender, EventArgs e)
    {
      dataGridView1.Rows.Clear();
      OpenFileDialog JsonDialog = new OpenFileDialog();
      JsonDialog.Filter = "Json files (*.json)|*.json|Text files (*.txt)|*.txt";
      JsonDialog.InitialDirectory = @"";
      JsonDialog.Title = "Выбирите файл JSON";


      if (JsonDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        JsonFileName = JsonDialog.FileName;
        DB_PATH = JsonDialog.FileName;

        try
        {
            Json_worker();

        }
        catch (Exception ex)
        {

          MessageBox.Show(ex.Message);
        }

      }
      oPENToolStripMenuItem.Enabled = false;
      btnOpenTxt.Enabled = true;
      btnCompareItem.Enabled = false;
      SaveJson.Enabled = false;

    }



    private void txtbxSearch_TextChanged(object sender, EventArgs e)
    {
      Filter();

    }



    //Открытие txt

    private void btnOpenTxt_Click(object sender, EventArgs e)
    {

      OpenFileDialog txtDialog = new OpenFileDialog();
      txtDialog.Filter = "Text files (*.txt)|*.txt";
      txtDialog.InitialDirectory = @"";
      txtDialog.Title = "Выбирите txt файл";


      if (txtDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        TxtFileName = txtDialog.FileName;
        TXT_PATH = txtDialog.FileName;

        try
        {
          string[] lines = File.ReadAllLines(TXT_PATH, Encoding.Default);
        
          for (int i = 0; i < lines.Length; i++)
          {
            dataGridView1.Rows.Add();
            dataGridView1.Rows[i].Cells[5].Value = lines[i].ToLower();
            TxtCollection.Add(lines[i].ToLower());
          }

          oPENToolStripMenuItem.Enabled = false;
          btnOpenTxt.Enabled = false;
          btnCompareItem.Enabled = true;
          SaveJson.Enabled = false;
        }
        catch (Exception ex)
        {

          MessageBox.Show(ex.Message);
        }

      }
    }


    JArray AddArr = new JArray();
    JArray SuperArr = new JArray();

    private void btnCompareItem_Click(object sender, EventArgs e)
    {

      dataGridView1.Rows.Clear();


      IEnumerable<string> onlyInFirstSet = JsonCollection.Except(TxtCollection);

      List<string> ClearTxt = new List<string>();

      foreach (var item in onlyInFirstSet)
      {
        ClearTxt.Add(item);

      }


      foreach (var item in a)
      {
        var newArr = item;
        foreach (var it in ClearTxt)
        {
          var arrTrue = it;
          var arrFalse = newArr[1].ToString();
          if (arrTrue == arrFalse)
          {
            string Id = null;
            for (int j = 0; j < dataGridView1.Rows.Count; j++)
              Id = Convert.ToString(dataGridView1.Rows[j].Cells[0].Value = j + 1);
            var arr11 = newArr[0].ToString();

            var arr22 = newArr[1].ToString();

            var arr33 = newArr[2].ToString();

            JToken testArr = new JArray( arr11, arr22, arr33);

            string empty = "";
            string[] arr = new string[] {Id, empty, arr11, arr22, arr33 };
            

            AddArr.Add(testArr);
            
            //ConvertToJson.Add(arr);
   
            dataGridView1.Rows.Add(arr);


          }
        }

      }

      oPENToolStripMenuItem.Enabled = false;
      btnOpenTxt.Enabled = false;
      btnCompareItem.Enabled = false;
      SaveJson.Enabled = true;


    }

    private void SaveJson_Click(object sender, EventArgs e)
    {

      dataGridView1.Rows.Clear();

      string final = Convert.ToString(AddArr);


      dynamic result = new JObject();
      result.devices = AddArr;

      JArray done = new JArray(result);




      try
      {
        SaveFileDialog saveDialog = new SaveFileDialog();
        saveDialog.Filter = "Json files (*.json)|*.json|Text files (*.txt)|*.txt";
        saveDialog.FileName = Path.GetFileNameWithoutExtension(JsonFileName);
        if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {

          File.WriteAllText(saveDialog.FileName, done.ToString());


          using (StreamWriter file = File.CreateText(saveDialog.FileName))
          using (JsonTextWriter writer = new JsonTextWriter(file))
          {
            done.WriteTo(writer);
          }

          MessageBox.Show("Export прошел успешно");

        }

      }    
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
      oPENToolStripMenuItem.Enabled = true;
      btnOpenTxt.Enabled = false;
      btnCompareItem.Enabled = false;
      SaveJson.Enabled = false;

    }
  }

}
