﻿namespace JSON_PARSER
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.oPENToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.btnOpenTxt = new System.Windows.Forms.ToolStripMenuItem();
      this.btnCompareItem = new System.Windows.Forms.ToolStripMenuItem();
      this.txtbxSearch = new System.Windows.Forms.TextBox();
      this.lbSearch = new System.Windows.Forms.Label();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.array = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.val1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.val2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.val3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.txtValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SaveJson = new System.Windows.Forms.Button();
      this.contextMenuStrip1.SuspendLayout();
      this.menuStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.SuspendLayout();
      // 
      // contextMenuStrip1
      // 
      this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem});
      this.contextMenuStrip1.Name = "contextMenuStrip1";
      this.contextMenuStrip1.Size = new System.Drawing.Size(122, 26);
      // 
      // открытьToolStripMenuItem
      // 
      this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
      this.открытьToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
      this.открытьToolStripMenuItem.Text = "Открыть";
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oPENToolStripMenuItem,
            this.btnOpenTxt,
            this.btnCompareItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(784, 24);
      this.menuStrip1.TabIndex = 1;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // oPENToolStripMenuItem
      // 
      this.oPENToolStripMenuItem.Image = global::JSON_PARSER.Properties.Resources.open_folder_outline;
      this.oPENToolStripMenuItem.Name = "oPENToolStripMenuItem";
      this.oPENToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
      this.oPENToolStripMenuItem.Text = "OPEN";
      this.oPENToolStripMenuItem.Click += new System.EventHandler(this.oPENToolStripMenuItem_Click);
      // 
      // btnOpenTxt
      // 
      this.btnOpenTxt.Image = global::JSON_PARSER.Properties.Resources.open_folder_outline;
      this.btnOpenTxt.Name = "btnOpenTxt";
      this.btnOpenTxt.Size = new System.Drawing.Size(79, 20);
      this.btnOpenTxt.Text = "OpenTxt";
      this.btnOpenTxt.Click += new System.EventHandler(this.btnOpenTxt_Click);
      // 
      // btnCompareItem
      // 
      this.btnCompareItem.Name = "btnCompareItem";
      this.btnCompareItem.Size = new System.Drawing.Size(68, 20);
      this.btnCompareItem.Text = "Compare";
      this.btnCompareItem.Click += new System.EventHandler(this.btnCompareItem_Click);
      // 
      // txtbxSearch
      // 
      this.txtbxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.txtbxSearch.Location = new System.Drawing.Point(62, 335);
      this.txtbxSearch.Name = "txtbxSearch";
      this.txtbxSearch.Size = new System.Drawing.Size(174, 20);
      this.txtbxSearch.TabIndex = 4;
      this.txtbxSearch.Tag = "";
      this.txtbxSearch.TextChanged += new System.EventHandler(this.txtbxSearch_TextChanged);
      // 
      // lbSearch
      // 
      this.lbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lbSearch.AutoSize = true;
      this.lbSearch.Location = new System.Drawing.Point(12, 338);
      this.lbSearch.Name = "lbSearch";
      this.lbSearch.Size = new System.Drawing.Size(44, 13);
      this.lbSearch.TabIndex = 5;
      this.lbSearch.Text = "Search:";
      // 
      // dataGridView1
      // 
      this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.array,
            this.val1,
            this.val2,
            this.val3,
            this.txtValue});
      this.dataGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.dataGridView1.Location = new System.Drawing.Point(0, 24);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.dataGridView1.Size = new System.Drawing.Size(784, 279);
      this.dataGridView1.TabIndex = 2;
      // 
      // ID
      // 
      this.ID.HeaderText = "ID";
      this.ID.Name = "ID";
      // 
      // array
      // 
      this.array.HeaderText = "array";
      this.array.Name = "array";
      // 
      // val1
      // 
      this.val1.HeaderText = "val1";
      this.val1.Name = "val1";
      // 
      // val2
      // 
      this.val2.HeaderText = "val2";
      this.val2.Name = "val2";
      // 
      // val3
      // 
      this.val3.HeaderText = "val3";
      this.val3.Name = "val3";
      // 
      // txtValue
      // 
      this.txtValue.HeaderText = "txtValue";
      this.txtValue.Name = "txtValue";
      // 
      // SaveJson
      // 
      this.SaveJson.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.SaveJson.AutoSize = true;
      this.SaveJson.Location = new System.Drawing.Point(663, 327);
      this.SaveJson.Name = "SaveJson";
      this.SaveJson.Size = new System.Drawing.Size(109, 34);
      this.SaveJson.TabIndex = 7;
      this.SaveJson.Text = "SAVE JSON";
      this.SaveJson.UseVisualStyleBackColor = true;
      this.SaveJson.Click += new System.EventHandler(this.SaveJson_Click);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(784, 373);
      this.Controls.Add(this.SaveJson);
      this.Controls.Add(this.lbSearch);
      this.Controls.Add(this.txtbxSearch);
      this.Controls.Add(this.dataGridView1);
      this.Controls.Add(this.menuStrip1);
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "Form1";
      this.Text = "JsonParser";
      this.contextMenuStrip1.ResumeLayout(false);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem oPENToolStripMenuItem;
    private System.Windows.Forms.TextBox txtbxSearch;
    private System.Windows.Forms.Label lbSearch;
    private System.Windows.Forms.DataGridView dataGridView1;
    private System.Windows.Forms.ToolStripMenuItem btnOpenTxt;
    private System.Windows.Forms.ToolStripMenuItem btnCompareItem;
    private System.Windows.Forms.Button SaveJson;
    private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    private System.Windows.Forms.DataGridViewTextBoxColumn array;
    private System.Windows.Forms.DataGridViewTextBoxColumn val1;
    private System.Windows.Forms.DataGridViewTextBoxColumn val2;
    private System.Windows.Forms.DataGridViewTextBoxColumn val3;
    private System.Windows.Forms.DataGridViewTextBoxColumn txtValue;
  }
}

