﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.ComponentModel;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Threading;
using System.Data;
using System.Xml;

namespace JSON_PARSER
{
  class myExcel
  {

    public static Excel.Workbook Book = null;
    public static Excel.Application App = null;
    public static Excel.Worksheet Sheet = null;
    public static Excel.Range Range = null;
    public static int lastRow = 0;
    

    public static void CloseExcel()
    {

      try
      {
        Book.Saved = true;//Сохраняем перед закрытием
        App.Quit();
      }
      catch (Exception)
      {
      }

    }

  }
}
